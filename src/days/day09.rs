extern crate nom;

use std::time::Instant;

use crate::etc::utils::input;

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("9".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let sum: i32 = input
        .trim()
        .split("\n")
        .map(|l| {
            let parsed_line = l.split(" ")
                .map(|n| n.parse::<i32>().expect("an integer"))
                .collect::<Vec<i32>>();
            extrapolate(&parsed_line)
        })
        .sum();
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let sum: i32 = input
        .trim()
        .split("\n")
        .map(|l| {
            let parsed_line = l.split(" ")
                .map(|n| n.parse::<i32>().expect("an integer"))
                .collect::<Vec<i32>>();
            extrapolate_front(&parsed_line)
        })
        .sum();

    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn extrapolate(vec: &Vec<i32>) -> i32 {
    if vec.iter().all(|n| *n == 0) {
        return 0;
    }
    let mut pat = Vec::new();
    for w in vec.windows(2) {
        pat.push(w[1] - w[0]);
    }
    return vec.last().unwrap() + extrapolate(&pat);
}

fn extrapolate_front(vec: &Vec<i32>) -> i32 {
    if vec.iter().all(|n| *n == 0) {
        return 0;
    }
    let mut pat = Vec::new();
    for w in vec.windows(2) {
        pat.push(w[1] - w[0]);
    }
    return vec.first().unwrap() - extrapolate_front(&pat);
}
