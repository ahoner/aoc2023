extern crate nom;

use nom::{
    bytes::complete::{
        tag,
        take_until
    },
    character::complete::{self},
    multi::separated_list1,
    IResult,
    sequence::{
        preceded,
        separated_pair
    },
    combinator::map,
};

use std::{
    time::Instant,
    ops::Range,
};

use crate::etc::utils::input;

#[derive(Debug)]
struct MapGroup {
    src: Range<u64>,
    dest: Range<u64>,

}

impl MapGroup {
    fn map_num(&self, num: u64) -> Option<u64> {
        if !self.src.contains(&num) {
            return None
        }
        return Some(self.dest.start + (num - self.src.start));
    }

    fn src_intersect(&self, range: Range<u64>) -> bool {
        return self.src.start < range.end && range.start < self.src.end;
    }

    fn map_range(&self, range: Range<u64>) -> (Vec<Range<u64>>, Range<u64>) {
        if self.src.contains(&range.start) && self.src.contains(&(range.end - 1)) {
            return (
                Vec::from([]),
                self.map_num(range.start).unwrap()..self.map_num(range.end - 1).unwrap() + 1
            );
        } else if !self.src.contains(&range.start) && self.src.contains(&(range.end - 1)) {
            return (
                Vec::from([
                    range.start..self.src.start,
                ]),
                self.map_num(self.src.start).unwrap()..self.map_num(range.end - 1).unwrap() + 1
            );
        } else if self.src.contains(&range.start) && !self.src.contains(&(range.end - 1)) {
            return (
                Vec::from([
                    self.src.end..range.end,
                ]),
                self.map_num(range.start).unwrap()..self.map_num(self.src.end - 1).unwrap() + 1
            )
        } else {
            if range.start < self.src.start && self.src.end - 1 < range.end - 1 {
                return (
                    Vec::from([
                        range.start..self.src.start,
                        self.src.end..range.end
                    ]),
                    self.map_num(self.src.start).unwrap()..self.map_num(self.src.end - 1).unwrap() + 1,
                )
            }
            panic!("we shoulden't get herere");
        }
    }
}

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("5".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let (_, (seeds, seed_mappings)) = parse_part_1(input).expect("a valid parse");
    let mut lowest_location = u64::MAX;
    for s in seeds.into_iter() {
        let mut mapped: Option<u64> = Some(s);
        for mapping in seed_mappings.iter() {
            match mapping.iter().find(|mg| mg.src.contains(&mapped.unwrap())) {
                Some(m) => {
                    mapped = m.map_num(mapped.unwrap());
                },
                None => ()
            }
        }
        if mapped.unwrap() < lowest_location {
            lowest_location = mapped.unwrap();
        }
    }

    println!("{}", lowest_location);

    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let (_, (seed_ranges, seed_mappings)) = parse_part_2(input).expect("a valid parse");
    let mut item_queue = Vec::from(seed_ranges);
    for mp in seed_mappings.iter() {
        let mut next_list: Vec<Range<u64>> = Vec::from([]);
        while !item_queue.is_empty() {
            let current = item_queue.pop().unwrap();
            let mut has_match = false;
            for m in mp.iter() {
                if m.src_intersect(current.clone()) {
                    has_match = true;
                    let (unmapped, mapped) = m.map_range(current.clone());
                    next_list.push(mapped);
                    for um in unmapped.iter() {
                        item_queue.push(um.clone());
                    }
                    break;
                }
            }
            if !has_match {
                next_list.push(current.clone());
            }
        }
        item_queue = next_list.clone();
    }

    let lowest_location = item_queue.into_iter()
        .fold(u64::MAX, |acc, range| {
            if range.start < acc { range.start } else { acc }
        });
    println!("{}", lowest_location);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn parse_seeds(input: &str) -> IResult<&str, Vec<u64>> {
    let (input, _) = tag("seeds: ")(input)?;
    let (input, list) = separated_list1(tag(" "), complete::u64)(input)?;
    Ok((input, list))
}

fn parse_seed_ranges(input: &str) -> IResult<&str, Vec<Range<u64>>> {
    let (input, _) = tag("seeds: ")(input)?;
    let (input, list) = separated_list1(tag(" "), parse_seed_range)(input)?;
    Ok((input, list))
}

fn parse_seed_range(input: &str) -> IResult<&str, Range<u64>> {
    let mut parser = map(
        separated_pair(complete::u64, tag(" "), complete::u64),
        |(start, dist)| start..start + dist,
    );
    parser(input)
}

fn parse_maps(input: &str) -> IResult<&str, Vec<MapGroup>> {
    let (input, _) = take_until("map:\n")(input)?;
    let mut parser= preceded(
        tag("map:\n"),
        separated_list1(tag("\n"), parse_map)
    );
    let (input, mapping_groups) = parser(input)?;
    Ok((input, mapping_groups))
}

fn parse_map(input: &str) -> IResult<&str, MapGroup> {
    let (input, num_list) = separated_list1(tag(" "), complete::u64)(input)?;
    Ok((input, MapGroup {
        src: num_list[1]..num_list[1] + num_list[2],
        dest: num_list[0]..num_list[0] + num_list[2]
    }))
}

fn parse_part_1(input: &str) -> IResult<&str, (Vec<u64>, Vec<Vec<MapGroup>>)> {
    let (input, seeds) = parse_seeds(input)?;
    let (input, seed_mappings) = separated_list1(tag("\n\n"), parse_maps)(input)?;
    Ok((input, (seeds, seed_mappings)))
}

fn parse_part_2(input: &str) -> IResult<&str, (Vec<Range<u64>>, Vec<Vec<MapGroup>>)> {
    let (input, seed_ranges) = parse_seed_ranges(input)?;
    let (input, seed_mappings) = separated_list1(tag("\n\n"), parse_maps)(input)?;
    Ok((input, (seed_ranges, seed_mappings)))
}
