use std::time::{Instant};
use std::collections::HashMap;

use crate::etc::utils::input;

const MAX_RED: u32 = 12;
const MAX_GREEN: u32 = 13;
const MAX_BLUE: u32 = 14;

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("2".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let sum: u32 = input
        .trim()
        .split("\n")
        .fold(0, |inc, l| {
            let l: Vec<&str> = l.split(": ").collect::<Vec<_>>();
            let id: u32 = l[0].split(" ").collect::<Vec<_>>()[1].parse::<u32>().unwrap();
            let games: Vec<&str> = l[1].split("; ").collect::<Vec<_>>();
            if eval_games_validity(games) {
                inc + id
            } else {
                inc
            }
        });
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let power: u32 = input
        .trim()
        .split("\n")
        .fold(0, |inc, l| {
            let l: Vec<&str> = l.split(": ").collect::<Vec<_>>();
            let games: Vec<&str> = l[1].split("; ").collect::<Vec<_>>();
            eval_game_power(games) + inc
        });
    println!("{}", power);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn eval_games_validity(games: Vec<&str>) -> bool {
    games.iter()
        .fold(true, |inc, g| {
            g.split(", ")
                .collect::<Vec<_>>()
                .iter()
                .fold(true, |inc, p| {
                    let p: Vec<&str> = p.split(" ").collect::<Vec<_>>();
                    let count: u32 = p[0].parse::<u32>().unwrap();
                    match p[1] {
                        "red" => MAX_RED >= count && inc,
                        "green" => MAX_GREEN >= count && inc,
                        "blue" => MAX_BLUE >= count && inc,
                        _ => panic!("parsing error in pick!"),
                    }
                }) && inc
        })
}

fn eval_game_power(games: Vec<&str>) -> u32 {
    let mut min_blocks_map = HashMap::from([
        ("red", 0u32),
        ("green", 0u32),
        ("blue", 0u32),
    ]);
    for g in games {
        for p in g.split(", ").collect::<Vec<_>>() {
            let p: Vec<&str> = p.split(" ").collect::<Vec<_>>();
            let count: u32 = p[0].parse::<u32>().unwrap();
            match p[1] {
                "red" => {
                    if &count >= min_blocks_map.get("red").unwrap() {
                        min_blocks_map.insert("red", count);
                    }
                },
                "blue" => {
                    if &count >= min_blocks_map.get("blue").unwrap() {
                        min_blocks_map.insert("blue", count);
                    }
                },
                "green" => {
                    if &count >= min_blocks_map.get("green").unwrap() {
                        min_blocks_map.insert("green", count);
                    }
                },
                _ => panic!("prasing error in pick!"),
            };
        }
    }
    min_blocks_map.get("red").unwrap()
        * min_blocks_map.get("blue").unwrap()
        * min_blocks_map.get("green").unwrap()
}
