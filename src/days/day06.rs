extern crate nom;

use std::{time::Instant, ops::Range};

use nom::{
    IResult,
    bytes::complete::{
        tag,
        take_till
    },
    character::complete::{self},
    multi::{separated_list1, many1},
    sequence::separated_pair,
};

use crate::etc::utils::input;

#[derive(Debug)]
struct RaceRules {
    time: u64,
    dist: u64
}

impl RaceRules {
    fn calc_win_range(&self) -> Range<u64> {
        // Express win range as area of a square time_held * (Total_race_time - time_held)
        // Set that equal to the total distance, find the intersect points and do the necessary roundin to find the win ranges
        // next two lines are just the quadradic formula
        let mut low = (self.time as f64 / 2f64) - (((self.time as f64).powi(2) - 4f64 * self.dist as f64) as f64).sqrt() / 2f64;
        let mut high = (self.time as f64 / 2f64) + (((self.time as f64).powi(2) - 4f64 * self.dist as f64) as f64).sqrt() / 2f64;
        if low.fract() == 0.0 {
            low = low + 1f64;
        }
        if high.fract() == 0.0 {
            high = high - 1f64;
        }
        low.ceil() as u64..high.floor() as u64
    }
}

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("6".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let (_, race_rules_list) = parse_part_1(input).expect("a valid parse");
    let num_winning: u64 = race_rules_list
        .iter()
        .map(|rr| rr.calc_win_range().count() as u64 + 1)
        .fold(1, |acc, n| acc * n);

    println!("{}", num_winning);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f64 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let (_, race_rules) = parse_part_2(input).expect("a valid parse");
    let win_range = race_rules.calc_win_range();
    let num_winning = win_range.end - win_range.start + 1;

    println!("{}", num_winning);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f64 / 1000.0);
}

fn parse_line(input: &str) -> IResult<&str, Vec<u64>> {
    let mut parser = separated_pair(
        take_till(|c: char| c.is_whitespace()),
        many1(tag(" ")),
        separated_list1(many1(tag(" ")), complete::u64)
    );
    let (input, (_, num_list)) = parser(input)?;
    Ok((input, num_list))
}

fn parse_part_1(input: &str) -> IResult<&str, Vec<RaceRules>> {
    let mut parser = separated_pair(
        parse_line,
        tag("\n"),
        parse_line
    );
    let (input, (time_list, dist_list)) = parser(input)?;
    let mut race_rules_list = Vec::from([]);
    for (i, t) in time_list.iter().enumerate() {
        race_rules_list.push(RaceRules {
            time: *t,
            dist: dist_list[i]
        });
    }
    Ok((input, race_rules_list))
}

fn parse_part_2(input: &str) -> IResult<&str, RaceRules> {
    let mut parser = separated_pair(
        parse_line,
        tag("\n"),
        parse_line
    );
    let (input, (time_list, dist_list)) = parser(input)?;
    Ok((input, RaceRules {
        time: time_list.iter().map(|n| n.to_string()).collect::<Vec<String>>().join("").parse::<u64>().unwrap(),
        dist: dist_list.iter().map(|n| n.to_string()).collect::<Vec<String>>().join("").parse::<u64>().unwrap(),
    }))
}