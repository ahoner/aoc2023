use std::time::{Instant};
use std::collections::HashMap;

use crate::etc::utils::input;

const BASE_TEN: u32 = 10;
const ADJ_MATRIX: [(isize, isize);9] = [
    (-1, -1), (0, -1), (1, -1),
    (-1, 0), (0, 0), (1, 0),
    (-1, 1), (0, 1), (1, 1)
]; 

#[derive(Hash, Eq, PartialEq)]
struct Coord {
    x: isize,
    y: isize,
}

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("3".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let schematic: Vec<Vec<char>> = input
        .trim()
        .split("\n")
        .map(|l| l.chars().collect::<Vec<char>>())
        .collect();
    let mut sum: isize = 0;
    for (i, line) in schematic.iter().enumerate() {
        let i = i as isize;
        let mut current_num: Vec<char> = Vec::new();
        let mut is_part_num: bool = false;
        for (j, c) in line.iter().enumerate(){
            let j = j as isize;
            if c.is_digit(BASE_TEN) {
                // continue building number and check each digit for adjacencies
                current_num.push(*c);
                if !is_part_num {
                    is_part_num = is_adjacent_to_symbol(j, i, &schematic).is_some();
                }
            } else {
                // reached a non digit, if number adjacent to symbol add to the sum and reset
                if is_part_num {
                    let num = String::from_iter(current_num).parse::<isize>().unwrap();
                    sum += num;
                }
                current_num = Vec::new();
                is_part_num = false;
            }
        }
        // reached the end of line do same check above but no need to reset vars
        if is_part_num {
            let num = String::from_iter(current_num).parse::<isize>().unwrap();
            sum += num;
        }
    }
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let schematic: Vec<Vec<char>> = input
        .trim()
        .split("\n")
        .map(|l| l.chars().collect::<Vec<char>>())
        .collect();
    let mut gear_map: HashMap<Coord, Vec<isize>> = HashMap::new();
    for (i, line) in schematic.iter().enumerate() {
        let i = i as isize;
        let mut current_num: Vec<char> = Vec::new();
        let mut is_part_num: bool = false;
        let mut gear_adjacent: Option<Coord> = None;
        for (j, c) in line.iter().enumerate(){
            let j = j as isize;
            // continue building number and check each digit for adjacencies
            if c.is_digit(BASE_TEN) {
                current_num.push(*c);
                if !is_part_num {
                    // map gear coordinates to adjacent numbers
                    match is_adjacent_to_symbol(j, i, &schematic) {
                        Some(coord) => {
                            is_part_num = true;
                            if gear_adjacent.is_none() {
                                if schematic[coord.y as usize][coord.x as usize] == '*' {
                                    gear_adjacent = Some(Coord { x: coord.x, y: coord.y });
                                }
                            }
                        },
                        None => (),
                    };
                }
            } else {
                // reached non number, if gear adjacent add number to gear map
                match gear_adjacent {
                    Some(gear_coord) => {
                        let num = String::from_iter(current_num).parse::<isize>().unwrap();
                        if gear_map.contains_key(&gear_coord) {
                            gear_map.get_mut(&gear_coord).unwrap().push(num);
                        } else {
                            gear_map.insert(Coord { x: gear_coord.x, y: gear_coord.y }, vec![num]);
                        }
                    },
                    None => ()
                }
                current_num = Vec::new();
                is_part_num = false;
                gear_adjacent = None;
            }
        }
        // same for end of line
        if is_part_num {
            match gear_adjacent {
                Some(gear_coord) => {
                    let num = String::from_iter(current_num).parse::<isize>().unwrap();
                    if gear_map.contains_key(&gear_coord) {
                        gear_map.get_mut(&gear_coord).unwrap().push(num);
                    } else {
                        gear_map.insert(Coord { x: gear_coord.x, y: gear_coord.y }, vec![num]);
                    }
                },
                None => ()
            }
        }
    }
    let sum = gear_map.into_values()
        .filter(|v| v.len() == 2)
        .map(|v| v[0] * v[1])
        .fold(0, |inc, n| inc + n);
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn is_adjacent_to_symbol(x: isize, y: isize, schematic: &Vec<Vec<char>>) -> Option<Coord> {
    let x_max = schematic[0].len() as isize;
    let y_max = schematic.len() as isize;
    for (dx, dy) in ADJ_MATRIX {
        if x + dx >= 0 &&
            x_max > x + dx &&
            y + dy >= 0 &&
            y_max > y + dy {
                let current_char = schematic[(y + dy) as usize][(x + dx) as usize];
                if !current_char.is_digit(BASE_TEN) && current_char != '.' {
                    return Some(Coord { x: x + dx, y: y + dy })
                }
            }
    }
    return None;
}
