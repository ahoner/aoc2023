use std::time::{Instant};

use regex::Regex;

use crate::etc::utils::input;

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("1".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let sum: i32 = input 
        .trim()
        .split("\n")
        .fold(0, |inc, s| {
            let numbers: Vec<char> = s.chars()
                .filter(|c| c.is_digit(10))
                .collect();
            if numbers.len() == 0 {
                return inc;
            }
            let formatted_string: String = format!("{}{}", numbers[0], numbers.last().unwrap());
            return inc + formatted_string.parse::<i32>().unwrap();
        });

    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let sum: i32 = input
        .trim()
        .split("\n")
        .fold(0, |inc, s| {
            // checking list in reverse to capture overlapping sequenses like oneight or twone
            // Doesn't seem like rust allows overlapping capture groups in regex otherwise that approach would have been taken
            let whitelist = Regex::new(r"(one|two|three|four|five|six|seven|eight|nine|\d)").unwrap();
            let whitelist_reverse = Regex::new(r"(\d|enin|thgie|neves|xis|evif|ruof|eerht|owt|eno)").unwrap();
            let reverse: &str = &s.chars().rev().collect::<String>();
            let mat: &str = whitelist.find(s).unwrap().as_str();
            let mat_reverse: &str = whitelist_reverse.find(reverse).unwrap().as_str();
            let mat: i32 = match mat {
                "one" => 1,
                "two" => 2,
                "three" => 3,
                "four" => 4,
                "five" => 5,
                "six" => 6,
                "seven" => 7,
                "eight" => 8,
                "nine" => 9,
                _ => {
                    if mat.is_empty() {
                        0
                    } else {
                        mat.parse::<i32>().unwrap()
                    }
                },
            };
            let mat_reverse: i32 = match mat_reverse {
                "eno" => 1,
                "owt" => 2,
                "eerht" => 3,
                "ruof" => 4,
                "evif" => 5,
                "xis" => 6,
                "neves" => 7,
                "thgie" => 8,
                "enin" => 9,
                _ => {
                    if mat_reverse.is_empty() {
                        0
                    } else {
                        mat_reverse.parse::<i32>().unwrap()
                    }
                },
            };
            let formatted_string: String = format!("{}{}", mat, mat_reverse);
            return inc + formatted_string.parse::<i32>().unwrap();
        });
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
