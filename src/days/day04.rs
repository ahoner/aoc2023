use std::time::{Instant};
use std::collections::VecDeque;

use crate::etc::utils::input;

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("4".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let score = input.trim()
        .split("\n")
        .fold(0, |inc, l| {
            let numbers_str: Vec<&str> = l.split(": ")
                .collect::<Vec<&str>>()[1]
                .split(" | ")
                .collect::<Vec<&str>>();
            let winners: Vec<u32> = numbers_str[0]
                .split(" ")
                .filter(|n| !n.is_empty())
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>();
            let card: Vec<u32> = numbers_str[1]
                .split(" ")
                .filter(|n| !n.is_empty())
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>();
            inc + winners.into_iter()
                .fold(0, |inc, w| {
                    let is_winner: bool = card.contains(&w);
                    if is_winner && inc == 0 {
                        1
                    } else if is_winner && inc > 0 {
                        inc * 2
                    } else {
                        inc
                    }
                })
        });
    println!("{}", score);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let score_map = input.trim()
        .split("\n")
        .map(|l| {
            let numbers_str: Vec<&str> = l.split(": ")
                .collect::<Vec<&str>>()[1]
                .split(" | ")
                .collect::<Vec<&str>>();
            let winners: Vec<u32> = numbers_str[0]
                .split(" ")
                .filter(|n| !n.is_empty())
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>();
            let card: Vec<u32> = numbers_str[1]
                .split(" ")
                .filter(|n| !n.is_empty())
                .map(|n| n.parse::<u32>().unwrap())
                .collect::<Vec<u32>>();
            winners.into_iter()
                .fold(0, |inc, w| {
                    let is_winner: bool = card.contains(&w);
                    if is_winner && inc == 0 {
                        1
                    } else if is_winner && inc > 0 {
                        inc * 2
                    } else {
                        inc
                    }
                })
        })
        .collect::<Vec<u32>>();
    let mut sum = 0;
    let mut queue = VecDeque::from((0..score_map.len()).collect::<Vec<usize>>());
    while !queue.is_empty() {
        let card: usize = queue.pop_front().unwrap();
        let score: u32 = score_map[card];
        sum += 1;
        if score == 0 {
            continue;
        }
        for j in 1..=score.ilog2() + 1 {
            queue.push_back(card + j as usize);
        }
    }
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
