extern crate nom;

use std::{time::Instant, collections::HashSet};

use crate::etc::utils::input;

#[derive(Copy, Clone, Debug)]
enum Dir {
    North,
    East,
    South,
    West
}

const NORTH_CON: &str = "|F7";
const EAST_CON: &str= "-J7";
const SOUTH_CON: &str = "|JL";
const WEST_CON: &str= "-FL";
const CORNER: &str= "FLJ7";

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("10".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let map: Vec<Vec<char>> = input
        .trim()
        .split("\n")
        .map(|l| l.chars().collect())
        .collect();

    let start = find_start(&map);
    let count = get_path_set(&start.unwrap(), &map).len() / 2;

    println!("{}", count);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let map: Vec<Vec<char>> = input
        .trim()
        .split("\n")
        .map(|l| l.chars().collect())
        .collect();

    let start = find_start(&map);
    let path_set = get_path_set(&start.unwrap(), &map);
    let mut count: u32 = 0;

    for (y, l) in map.iter().enumerate() {
        let mut prev_corner: Option<char> = None;
        let mut is_outside = true;
        for (x, c) in l.iter().enumerate() {
            if path_set.contains(&(x, y))
                && (c == &'|'
                    || prev_corner == Some('F') && c == &'J'
                    || prev_corner == Some('L') && c == &'7'
                ) {
                is_outside = !is_outside;
            }
            if CORNER.contains(*c) {
                prev_corner = Some(*c);
            }
            if !path_set.contains(&(x, y)) && !is_outside {
                count += 1;
            }
        }
    }

    println!("{}", count);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn find_start(map: &Vec<Vec<char>>) -> Option<(usize, usize)> {
    for (i, l) in map.iter().enumerate() {
        for (j, c) in l.iter().enumerate() {
            if c == &'S' {
                return Some((j, i));
            }
        }
    }
    None
}

fn find_start_dirs(start: &(usize, usize), map: &Vec<Vec<char>>) -> Vec<Dir> {
    let mut dirs: Vec<Dir> = vec![];
    let (x, y) = *start;

    if y > 0 && NORTH_CON.chars().any(|c| c == map[y - 1][x]) {
        dirs.push(Dir::North)
    }
    if x < map[0].len() - 1 && EAST_CON.chars().any(|c| c == map[y][x + 1]) {
        dirs.push(Dir::East)
    }
    if y < map.len() - 1 && SOUTH_CON.chars().any(|c| c == map[y + 1][x]) {
        dirs.push(Dir::South)
    }
    if x > 0 &&WEST_CON.chars().any(|c| c == map[y][x - 1]) {
        dirs.push(Dir::West)
    }
    dirs
} 

fn get_path_set(start: &(usize, usize), map: &Vec<Vec<char>>) -> HashSet<(usize, usize)> {
    let start_dirs = find_start_dirs(start, map);
    let mut cw_dir = start_dirs[0];
    let mut ccw_dir = start_dirs[1];
    let mut cw_pos = *start;
    let mut ccw_pos = *start;
    let mut path: HashSet<(usize,usize)> = HashSet::new();
    loop {
        if cw_pos == ccw_pos && cw_pos != *start {
            path.insert(cw_pos);
            break;
        }
        if cw_pos == *start {
            path.insert(cw_pos);
            match cw_dir {
                Dir::North => { cw_pos = (cw_pos.0, cw_pos.1 - 1); cw_dir = Dir::South },
                Dir::East => { cw_pos = (cw_pos.0 + 1, cw_pos.1); cw_dir = Dir::West },
                Dir::South => { cw_pos = (cw_pos.0, cw_pos.1 + 1); cw_dir = Dir::North },
                Dir::West => { cw_pos = (cw_pos.0 - 1, cw_pos.1); cw_dir = Dir::East },
            }
            match ccw_dir {
                Dir::North => { ccw_pos = (ccw_pos.0, ccw_pos.1 - 1); ccw_dir = Dir::South },
                Dir::East => { ccw_pos = (ccw_pos.0 + 1, ccw_pos.1); ccw_dir = Dir::West },
                Dir::South => { ccw_pos = (ccw_pos.0, ccw_pos.1 + 1); ccw_dir = Dir::North },
                Dir::West => { ccw_pos = (ccw_pos.0 - 1, ccw_pos.1); ccw_dir = Dir::East },
            }
        } else {
            path.insert(cw_pos);
            path.insert(ccw_pos);
            (cw_pos, cw_dir) = next_pos(&cw_pos, cw_dir, map);
            (ccw_pos, ccw_dir) = next_pos(&ccw_pos, ccw_dir, map);
        }
    }
    return path;
}

fn next_pos(current_pos: &(usize, usize), prev_dir: Dir, map: &Vec<Vec<char>>) -> ((usize, usize), Dir) {
    let current_char = map[current_pos.1][current_pos.0];
    match current_char {
        '|' => {
            match prev_dir {
                Dir::North => ((current_pos.0, current_pos.1 + 1), Dir::North),
                Dir::South => ((current_pos.0, current_pos.1 - 1), Dir::South),
                _ => panic!("Invalid Dir"),
            }
        },
        '-' => {
            match prev_dir {
                Dir::East => ((current_pos.0 - 1, current_pos.1), Dir::East),
                Dir::West => ((current_pos.0 + 1, current_pos.1), Dir::West),
                _ => panic!("Invalid Dir"),
            }
        },
        'L' => {
            match prev_dir {
                Dir::North => ((current_pos.0 + 1, current_pos.1), Dir::West),
                Dir::East => ((current_pos.0, current_pos.1 - 1), Dir::South),
                _ => panic!("Invalid Dir"),
            }
        },
        'J' => {
            match prev_dir {
                Dir::North => ((current_pos.0 - 1, current_pos.1), Dir::East),
                Dir::West => ((current_pos.0, current_pos.1 - 1), Dir::South),
                _ => panic!("Invalid Dir"),
            }
        },
        '7' => {
            match prev_dir {
                Dir::West => ((current_pos.0, current_pos.1 + 1), Dir::North),
                Dir::South => ((current_pos.0 - 1, current_pos.1), Dir::East),
                _ => panic!("Invalid Dir"),
            }
        },
        'F' => {
            match prev_dir {
                Dir::East => ((current_pos.0, current_pos.1 + 1), Dir::North),
                Dir::South => ((current_pos.0 + 1, current_pos.1), Dir::West),
                _ => panic!("Invalid Dir"),
            }
        },
        _ => panic!("Invalid Parse")
    }
}
