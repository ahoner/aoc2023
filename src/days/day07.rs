extern crate nom;

use std::{
    cmp::Ordering,
    collections::HashMap,
    time::Instant,
};

use crate::etc::utils::input;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum HandType {
    High,
    OnePair,
    TwoPair,
    Three,
    House,
    Four,
    Five
}

#[derive(Debug)]
struct Hand {
    hand: String,
    hand_map: HashMap<char, u32>,
    bid: u32
}

impl Hand {
    fn get_hand_type(&self) -> HandType {
        let vals: Vec<u32> = self.hand_map.values().map(|v| *v).collect();
        if vals.contains(&5) {
            HandType::Five
        } else if vals.contains(&4) {
            HandType::Four
        } else if vals.contains(&3) && vals.contains(&2) {
            HandType::House
        } else if vals.contains(&3) {
            HandType::Three
        } else if vals.contains(&2) && vals.len() == 3 {
            HandType::TwoPair
        } else if vals.contains(&2) && vals.len() == 4 {
            HandType::OnePair
        } else {
            HandType::High
        }
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.hand.cmp(&other.hand) == Ordering::Equal {
            return Ordering::Equal;
        }
        match self.get_hand_type().cmp(&other.get_hand_type()) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => {
                let cur_hand = self.hand.chars();
                let mut other_hand = other.hand.chars();
                for card in cur_hand {
                    let card_ordering = card_val(card).cmp(&card_val(other_hand.next().unwrap()));
                    if  card_ordering != Ordering::Equal {
                        return card_ordering
                    }
                }
                panic!("should never get here");
            }
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.hand == other.hand
    }
}

impl Eq for Hand {}

#[derive(Debug)]
struct WildHand {
    hand: String,
    hand_map: HashMap<char, u32>,
    bid: u32
}

impl WildHand {
    fn get_hand_type(&self) -> HandType {
        let vals: Vec<u32> = self.hand_map.values().map(|v| *v).collect();
        if vals.contains(&5) ||
            self.hand.contains('J') && self.hand_map.iter().any(|(c, v)| v + self.hand_map.get(&'J').unwrap() == 5 && c != &'J') {
            HandType::Five
        } else if vals.contains(&4) ||
            self.hand.contains('J') && self.hand_map.iter().any(|(c, v)| v + self.hand_map.get(&'J').unwrap() == 4 && c != &'J') {
            HandType::Four
        } else if vals.contains(&3) && vals.contains(&2) ||
            self.hand.contains('J') && vals.contains(&2) && vals.len() == 3 && *self.hand_map.get(&'J').unwrap() == 1 {
            HandType::House
        } else if vals.contains(&3) ||
            self.hand.contains('J') && vals.contains(&2) && vals.len() == 4 {
            HandType::Three
        } else if vals.contains(&2) && vals.len() == 3 {
            HandType::TwoPair
        } else if vals.contains(&2) && vals.len() == 4 ||
            self.hand.contains('J') && vals.len() == 5 {
            HandType::OnePair
        } else {
            HandType::High
        }
    }
}

impl Ord for WildHand {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.hand.cmp(&other.hand) == Ordering::Equal {
            return Ordering::Equal;
        }
        match self.get_hand_type().cmp(&other.get_hand_type()) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => {
                let cur_hand = self.hand.chars();
                let mut other_hand = other.hand.chars();
                for card in cur_hand {
                    let card_ordering = card_val_2(card).cmp(&card_val_2(other_hand.next().unwrap()));
                    if  card_ordering != Ordering::Equal {
                        return card_ordering
                    }
                }
                panic!("should never get here");
            }
        }
    }
}

impl PartialOrd for WildHand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for WildHand {
    fn eq(&self, other: &Self) -> bool {
        self.hand == other.hand
    }
}

impl Eq for WildHand {}

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("7".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut hands: Vec<Hand> = input
        .trim()
        .split("\n")
        .map(|l| {
            let (hand, bid) = l.trim().split_once(" ").unwrap();
            let mut hand_map: HashMap<char, u32> = HashMap::new();
            for c in hand.chars() {
                hand_map
                    .entry(c)
                    .and_modify(|c| *c += 1)
                    .or_insert(1);
            }
            Hand {
                hand: String::from(hand),
                hand_map: hand_map,
                bid: bid.parse().unwrap()
            }
        })
        .collect();
    hands.sort();

    let total_winnings = hands
        .into_iter()
        .enumerate()
        .fold(0, |acc, (i, h)| acc + h.bid * (i as u32 + 1));

    println!("{}", total_winnings);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut hands: Vec<WildHand> = input
        .trim()
        .split("\n")
        .map(|l| {
            let (hand, bid) = l.trim().split_once(" ").unwrap();
            let mut hand_map: HashMap<char, u32> = HashMap::new();
            for c in hand.chars() {
                hand_map
                    .entry(c)
                    .and_modify(|c| *c += 1)
                    .or_insert(1);
            }
            WildHand {
                hand: String::from(hand),
                hand_map: hand_map,
                bid: bid.parse().unwrap()
            }
        })
        .collect();

    hands.sort();

    let total_winnings = hands
        .into_iter()
        .enumerate()
        .fold(0, |acc, (i, h)| acc + h.bid * (i as u32 + 1));

    println!("{}", total_winnings);

    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn card_val(card: char) -> u32 {
    match card {
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'J' => 11,
        'T' => 10,
        _ => card.to_digit(10).unwrap()
    }
}

fn card_val_2(card: char) -> u32 {
    match card {
        'A' => 14,
        'K' => 13,
        'Q' => 12,
        'T' => 10,
        'J' => 1,
        _ => card.to_digit(10).unwrap()
    }
}
