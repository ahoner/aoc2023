extern crate nom;

use std::{time::Instant, collections::HashMap};

use nom::{
    IResult,
    sequence::{
        delimited,
        separated_pair
    },
    bytes::complete::{
        tag,
        take_till
    },
};

use crate::etc::utils::input;

pub fn run(is_vbrick: bool) {
    let input: String = input::fetch_input("8".to_owned(), is_vbrick);
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &str) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut input = input.trim().split("\n\n");
    let directions = input.next().unwrap();
    let map_str = input.next().unwrap().split("\n");

    let mut map: HashMap<&str, (&str, &str)> = HashMap::new();
    for l in map_str {
        let (_, (start, next)) = parse_map(l).expect("a valid parse");
        map.insert(start, next);
    }

    let mut current = "AAA";
    let mut current_directions = directions.clone().chars().into_iter();
    let mut i = 0;
    loop {
        let mut current_dir_option = current_directions.next();
        if current_dir_option.is_none() {
            current_directions = directions.clone().chars().into_iter();
            current_dir_option = current_directions.next();
        }
        let current_dir = current_dir_option.unwrap();
        if current == "ZZZ" {
            break;
        }
        let (left, right) = map.get(current).unwrap();
        if current_dir == 'L' {
            current = left;
        } else {
            current = right;
        }
        i += 1;
    }

    println!("{}", i);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &str) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut input = input.trim().split("\n\n");
    let directions = input.next().unwrap();
    let map_str = input.next().unwrap().split("\n");
    let mut current_positions: Vec<&str> = Vec::new();

    let mut map: HashMap<&str, (&str, &str)> = HashMap::new();
    for l in map_str {
        let (_, (start, next)) = parse_map(l).expect("a valid parse");
        if start.ends_with(|c: char| c == 'A') {
            current_positions.push(start);
        }
        map.insert(start, next);
    }

    let mut current_directions = directions.clone().chars().into_iter();
    let dists: Vec<u64> = current_positions
        .into_iter()
        .map(|p| {
            let mut i = 0;
            let mut current = p;
            loop {
                if current.ends_with(|c: char| c == 'Z') {
                    break;
                }
                let mut current_dir_option = current_directions.next();
                if current_dir_option.is_none() {
                    current_directions = directions.clone().chars().into_iter();
                    current_dir_option = current_directions.next();
                }
                let current_dir = current_dir_option.unwrap();
                let (left, right) = map.get(current).unwrap();
                if current_dir == 'L' {
                    current = left;
                } else {
                    current = right;
                }
                i += 1;
            }
            i
        })
        .collect();

    dbg!(dists.clone());
    println!("And then WolfRamAlpha cause its late and i don't fille like implementing LCM of a list :)");
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn parse_map(input: &str) -> IResult<&str, (&str, (&str, &str))> {
    let mut parser = separated_pair(
        take_till(|c: char| c.is_whitespace()),
        tag(" = "),
        parse_next_pair
    );
    let (input, (start, next)) = parser(input)?;
    Ok((input, (start, next)))
}

fn parse_next_pair(input: &str) -> IResult<&str, (&str, &str)> {
    let mut parser = delimited(
        tag("("),
        separated_pair(
            take_till(|c: char| c == ','),
            tag(", "),
            take_till(|c: char| c == ')')
        ),
        tag(")")
    );
    let (input, (left, right)) = parser(input)?;
    Ok((input, (left, right)))
}
