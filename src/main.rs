#![feature(drain_filter)]

mod days;
mod etc;

use days::{
    day01,
    day02,
    day03,
    day04,
    day05,
    day06,
    day07,
    day08,
    day09,
    day10,
};
use std::env;

fn main() {
    let args: Vec<String> = env::args()
        .skip(1)
        .collect();
    if args.len() == 0 {
        panic!("You must provide the day to run.");
    }

    let mut is_vbrick = false;
    for arg in args {
        if arg == "v" {
            is_vbrick = true;
            continue;
        }
        let func = match arg.as_str() {
            "1" => day01::run,
            "2" => day02::run,
            "3" => day03::run,
            "4" => day04::run,
            "5" => day05::run,
            "6" => day06::run,
            "7" => day07::run,
            "8" => day08::run,
            "9" => day09::run,
            "10" => day10::run,
            _ => panic!("Day not implemented."),
        };

        println!("\n--- Day {:02} ---", arg);
        func(is_vbrick);
    }
}